	Модель памяти Java — нечто, что оказывает влияние на то, как работает код любого java-разработчика. Тем не менее, довольно многие пренебрегают знанием этой важной темы, 
и порой наталкиваются на совершенно неожиданное поведение их приложений, которое объясняется именно особенностями устройства JMM. Возьмём для примера весьма распространённую 
и некорректную реализацию паттерна Double-checked locking:

public class Keeper {
    private Data data = null;
    
    public Data getData() {
        if(data == null) {
            synchronized(this) {
                if(data == null) {
                    data = new Data();
                }
            }
        }
        
        return data;
    }
}

	Люди, пишущие подобный код, пытаются добиться улучшения производительности, избегая блокировки, если значение уже было присвоено. К сожалению, эти люди не учитывают многих факторов, 
в результате проявления которых может случиться зомби-апокалипсис. Под катом я расскажу теорию и приведу примеры того, как что-то может пойти не так. Кроме того, 
как говорили в одном индийском фильме, «Мало знать, что не так. Нужно знать, как сделать так, чтобы было так». Потому и рецепты успеха вы также сможете найти дальше.

	Первая версия JMM появилась вместе с Java 1.0 в 1995 году. Это была первая попытка создать непротиворечивую и кросс-платформенную модель памяти. К сожалению, или к счастью, 
в ней было несколько серьёзных изъянов и непоняток. Одной из наиболее печальных проблем было отсутствие каких-либо гарантий для final полей. То есть, один поток мог создать объект 
с final-полем, а другой поток мог значения в этом final-поле не увидеть. Этому был подвержен даже класс java.lang.String. Кроме того, эта модель не давала компилятору 
возможности производить многие эффективные оптимизации, а при написании многопоточного кода сложно было быть уверенным в том, что он действительно будет работать так, как это ожидается.

	Потому в 2004 году в Java 5 появилась JSR 133, в которой были устранены недостатки первоначальной модели. О том, что получилось, мы и будем говорить.
	
	Atomicity
	Хотя многие это знают, считаю необходимым напомнить, что на некоторых платформах некоторые операции записи могут оказаться неатомарными. То есть, пока идёт запись значения одним потоком, 
другой поток может увидеть какое-то промежуточное состояние. За примером далеко ходить не нужно — записи тех же long и double, если они не объявлены как volatile, не обязаны быть атомарными 
и на многих платформах записываются в две операции: старшие и младшие 32 бита отдельно. (см. стандарт)

	Visibility
	В старой JMM у каждого из запущенных потоков был свой кеш (working memory), в котором хранились некоторые состояния объектов, которыми этот поток манипулировал. При некоторых условиях 
кеш синхронизировался с основной памятью (main memory), но тем не менее существенную часть времени значения в основной памяти и в кеше могли расходиться.
	В новой модели памяти от такой концепции отказались, потому что то, где именно хранится значение, вообще никому не интересно. Важно лишь то, при каких условиях один поток видит изменения, 
выполненные другим потоком. Кроме того, железо и без того достаточно умно, чтобы что-то кешировать, складывать в регистры и вытворять прочие операции.
	Важно отметить, что, в отличие от того же C++, «из воздуха» (out-of-thin-air) значения никогда не берутся: для любой переменной справедливо, что значение, наблюдаемое потоком, либо было 
ранее ей присвоено, либо является значением по умолчанию.

https://habr.com/ru/post/133981/
